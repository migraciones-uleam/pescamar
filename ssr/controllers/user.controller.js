var axios = require("axios").default;
const url = "http://api:5000/v1/api/usuario/";

async function getUsuario(req, res, next) {
    const usuario = await axios.get(url);
    res.render('usuario', { usuarios: usuario });
}

async function updateNuevoU(req, res, next) {
    const data = await axios.get(url ,);
    res.render('./nuevoU', { data: data.data });
}

async function postUsuario(req, res, next) {
    const { name, username, password } = req.body;
    await axios.post(url, { name, username, password });
    res.redirect('/usuario');
}

async function updateUsuarioForm(req, res, next) {
    const data = await axios.get(url + req.params.id);
    res.render('./usuarioForm', { data: data.data });
}

async function updateUsuario(req, res, next) {
    const { name, username, password } = req.body;
    await axios.patch(url + req.params.id, { name, username, password });
    res.redirect('/usuario');
}

async function deleteUsuario(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/usuario');
}

module.exports = {
    getUsuario,
    updateNuevoU,
    postUsuario,
    updateUsuarioForm,
    updateUsuario,
    deleteUsuario
}