var axios = require("axios").default;
const url = "http://api:5000/v1/api/lab/";

async function getLab(req, res, next) {
    const lab = await axios.get(url);
    res.render('lab', { labs: lab });
}
async function updateNuevoL(req, res, next) {
    const data = await axios.get(url ,);
    res.render('./nuevoL', { data: data.data });
}


async function postLab(req, res, next) {
    const { name, capacidad,observacion  } = req.body;
    await axios.post(url, { name, capacidad,observacion });
    res.redirect('/lab');
}

async function updateLaboratorioForm(req, res, next) {
    const data = await axios.get(url + req.params.id);
    res.render('./laboratorioForm', { data: data.data });
}

async function updateLab(req, res, next) {
    const { name, capacidad,observacion} = req.body;
    await axios.patch(url + req.params.id, { name, capacidad,observacion });
    res.redirect('/lab');
}

async function deleteLab(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/lab');
}

module.exports = {
    getLab,
    updateNuevoL,
    postLab,
    updateLaboratorioForm,
    updateLab,
    deleteLab
}