var axios = require("axios").default;
const url = "http://api:5000/v1/api/area/";

async function getArea(req, res, next) {
    const area = await axios.get(url);
    res.render('area', { areas: area });
}
async function updateNuevoA(req, res, next) {
    const data = await axios.get(url ,);
    res.render('./nuevoA', { data: data.data });
}

async function postArea(req, res, next) {
    const { codigo,area,descripcion } = req.body;
    await axios.post(url, { codigo,area,descripcion });
    res.redirect('/area');
}

async function updateAreaForm(req, res, next) {
    const data = await axios.get(url + req.params.id);
    res.render('./areaForm', { data: data.data });
}

async function updateArea(req, res, next) {
    const { codigo,area,descripcion } = req.body;
    await axios.patch(url + req.params.id, { codigo,area,descripcion});
    res.redirect('/area');
}

async function deleteArea(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/area');
}

module.exports = {
    getArea,
    updateNuevoA,
    postArea,
    updateAreaForm,
    updateArea,
    deleteArea
}