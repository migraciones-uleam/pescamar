var axios = require("axios").default;
const url = "http://api:5000/v1/api/equi/";

async function getEqui(req, res, next) {
    const equi = await axios.get(url);
    res.render('equi', { equis: equi });
}
async function updateNuevoE(req, res, next) {
    const data = await axios.get(url ,);
    res.render('./nuevoE', { data: data.data });
}

async function postEqui(req, res, next) {
    const { cantidad , nombre, descripcion } = req.body;
    await axios.post(url, { cantidad , nombre, descripcion });
    res.redirect('/equi');
}

async function updateEquipoForm(req, res, next) {
    const data = await axios.get(url + req.params.id);
    res.render('./equipoForm', { data: data.data });
}

async function updateEqui(req, res, next) {
    const {cantidad , nombre, descripcion } = req.body;
    await axios.patch(url + req.params.id, { cantidad , nombre, descripcion });
    res.redirect('/equi');
}

async function deleteEqui(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/equi');
}

module.exports = {
    getEqui,
    updateNuevoE,
    postEqui,
    updateEquipoForm,
    updateEqui,
    deleteEqui
}