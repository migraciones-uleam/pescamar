var express = require('express');
var router = express.Router();

const { UsuarioController } = require('../controllers/index');

router.get('/', UsuarioController.getUsuario);
router.get('/nuevoU', UsuarioController.updateNuevoU);
router.post('/', UsuarioController.postUsuario);
router.get('/:id', UsuarioController.updateUsuarioForm);
router.put('/:id', UsuarioController.updateUsuario);
router.delete('/:id', UsuarioController.deleteUsuario);

module.exports = router;
