var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'LAB A' });
});

// router.get('/usuario', function(req, res, next) {
//   res.render('usuarioForm', { title: 'LAB A' });
// });

// router.get('/area', function(req, res, next) {
//   res.render('areaForm', { title: 'LAB A' });
// });

// router.get('/equi', function(req, res, next) {
//   res.render('equipoForm', { title: 'LAB A' });
// });

module.exports = router;
