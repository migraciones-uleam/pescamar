const mongoose  =  require('mongoose') 
const { Schema } = mongoose;
const { genSaltSync, compareSync, hashSync } = require('bcryptjs');

const UsuarioSchema =   new Schema({
    name : { type:String, required: true },
    username : {type:String , required:true},
    password : {type:String , required:true}
})
UsuarioSchema.methods.toJSON = function(){
    let usuario = this.toObject();
    delete password;
    return usuario;
}
UsuarioSchema.methods.comparePassword = function(password){
    return compareSync( password,  this.password);
}

UsuarioSchema.pre('save', async  function(next){

    const usuario = this;
    if ( usuario.isModified("password") )
    {
        next();
    }

    const salt = genSaltSync(10);
    const hashPassword =   hashSync(usuario.password, salt);
    usuario.password = hashPassword;
    next();

})





module.exports = mongoose.model('usuario', UsuarioSchema);