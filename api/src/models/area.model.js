const mongoose  =  require('mongoose') 
const { Schema } = mongoose;


const AreaSchema =   new Schema({
   codigo : { type:String, required: true },
    area : {type:String , required:true},
    descripcion : {type:String , required:true},
})
AreaSchema.methods.toJSON = function(){
    let area = this.toObject();
    return area;
}
module.exports = mongoose.model('area', AreaSchema);