const { createContainer, asClass, asValue, asFunction } = require('awilix');

const config = require('../config');
const app = require('.');

//importar servicios
const { HomeService, LabService, UsuarioService, AreaService, EquiService} =  require('../services')

//importar controladores
const { HomeController, LabController, UsuarioController, AreaController, EquiController} = require('../controllers')

//importar rutas
const { HomeRoutes, LabRoutes, UsuarioRoutes, AreaRoutes, EquiRoutes   } = require('../routes/index.routes');
const Routes = require('../routes')

//modelos
const { Lab, Usuario, Area,Equi } = require('../models')

//repositorios

const { LabRepository, UsuarioRepository, AreaRepository, EquiRepository } = require('../repositories')


const container=  createContainer();

container
.register(
    {
        app: asClass(app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config)
    }
)
.register(
    {
        HomeService: asClass(HomeService).singleton(),
        LabService: asClass(LabService).singleton(),
        UsuarioService: asClass(UsuarioService).singleton(),
        EquiService: asClass(EquiService).singleton(),
        AreaService: asClass(AreaService).singleton()

    }
)
.register(
    {
        HomeController: asClass(HomeController.bind(HomeController)).singleton(),
        LabController: asClass(LabController.bind(LabController)).singleton(),
        AreaController: asClass(AreaController.bind(AreaController)).singleton(),
        EquiController: asClass(EquiController.bind(EquiController)).singleton(),
        UsuarioController: asClass(UsuarioController.bind(UsuarioController)).singleton()
    }
)
.register(
    {
        HomeRoutes: asFunction(HomeRoutes).singleton(),
        LabRoutes: asFunction(LabRoutes).singleton(),
        AreaRoutes: asFunction(AreaRoutes).singleton(),
        EquiRoutes: asFunction(EquiRoutes).singleton(),
        UsuarioRoutes: asFunction(UsuarioRoutes).singleton()
    }
)
.register(
    {
        Lab: asValue(Lab),
        Area: asValue(Area),
        Equi: asValue(Equi),
        Usuario: asValue(Usuario)
    }
)
.register(
    {
        LabRepository: asClass(LabRepository).singleton(),
        AreaRepository: asClass(AreaRepository).singleton(),
        EquiRepository: asClass(EquiRepository).singleton(),
        UsuarioRepository: asClass(UsuarioRepository).singleton()
    }
)


//scope      this

module.exports= container;
