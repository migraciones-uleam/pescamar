const BaseService =  require('./base.service')

let _equiRepository = null;

class EquiService extends BaseService
{
    constructor({EquiRepository})
    {
        super(EquiRepository);
        _equiRepository = EquiRepository;
    }
    async  getEquiByname(name)
     {
         return await  _equiRepository.getEquiByname(name);
    }

    
}
module.exports = EquiService;