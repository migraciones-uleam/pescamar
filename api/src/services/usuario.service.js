const BaseService =  require('./base.service')

let _usuarioRepository = null;

class UsuarioService extends BaseService
{
    constructor({UsuarioRepository})
    {
        super(UsuarioRepository);
        _usuarioRepository = UsuarioRepository;
    }
    async getUsuarioByUsuarioName(usuarioname)
    {
        return await  _usuarioRepository.getUsuarioByUsuarioName(usuarioname);
    }

}
module.exports = UsuarioService;