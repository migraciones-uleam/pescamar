const BaseRepository  = require('./base.repository')

let _area= null;

class AreaRepository extends BaseRepository
{
    constructor({Area})
    {
        super(Area);
        _area = Area;

    }
    async getAreaByname(name)
    {
        return await _area.findOne({name});
    }

}
module.exports = AreaRepository;