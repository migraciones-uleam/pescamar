const BaseRepository  = require('./base.repository')

let _usuario= null;

class UsuarioRepository extends BaseRepository
{
    constructor({Usuario})
    {
        super(Usuario);
        _usuario = Usuario;

    }
    async getUsuarioByUsuarioName(usuarioname)
    {
        return await _usuario.findOne({usuarioname});
    }

}
module.exports = UsuarioRepository;