module.exports = {
    LabRepository: require('./lab.repository'),
    AreaRepository: require('./area.repository'),
    EquiRepository: require('./equi.repository'),
    UsuarioRepository: require('./usuario.repository')
}
