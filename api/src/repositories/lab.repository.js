const BaseRepository  = require('./base.repository')

let _lab= null;

class LabRepository extends BaseRepository
{
    constructor({Lab})
    {
        super(Lab);
        _lab = Lab;

    }
    async getLabByname(name)
    {
        return await _lab.findOne({name});
    }

}
module.exports = LabRepository;
