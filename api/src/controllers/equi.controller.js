let _equiService = null;
class EquiController
{
    constructor({EquiService})
    {
        _equiService = EquiService;
    }
    //TODO: mEtodos CRUD
    //get
    async get(req,res)
    {
        const {equiId} = req.params;
        const equi=  await  _equiService.get(equiId);
        return  res.send(equi);
    }
    //getAll
    async getAll(req,res)
    {
        const equis =  await  _equiService.getAll();
        return res.send(equis);
    }
    //create
    async create(req, res)
    {
        const {body} = req;
        const createdEqui =  await  _equiService.create(body)
        res.send(createdEqui);
    }
    //update
    async update(req,res)
    {
        const {body} =  req;
        const { equiId } =  req.params;
        const updatedEqui =  await _equiService.update( equiId, body );
        res.send(updatedEqui);
    }
    //delete
    async delete(req,res)
    {
        const { equiId } = req.params;
        const deletedEqui =  await _equiService.delete(equiId);
        return res.send(deletedEqui);
    }

}
module.exports= EquiController;