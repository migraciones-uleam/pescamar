let _usuarioService = null;
class UsuarioController
{
    constructor({UsuarioService})
    {
        _usuarioService = UsuarioService;
    }
    //TODO: mEtodos CRUD
    //get
    async get(req,res)
    {
        const {usuarioId} = req.params;
        const usuario=  await  _usuarioService.get(usuarioId);
        return  res.send(usuario);
    }
    //getAll
    async getAll(req,res)
    {
        const usuarios =  await _usuarioService.getAll();
        return res.send(usuarios);
    }
    //create
    async create(req, res)
    {
        const {body} = req;
        const createdUsuario=  await _usuarioService.create(body)
        res.send(createdUsuario);
    }
    //update
    async update(req,res)
    {
        const {body} =  req;
        const { usuarioId } =  req.params;
        const updatedUsuario =  await _usuarioService.update( usuarioId, body );
        res.send(updatedUsuario);
    }
    //delete
    async delete(req,res)
    {
        const { usuarioId } = req.params;
        const deletedUsuario =  await _usuarioService.delete(usuarioId);
        return res.send(deletedUsuario);
    }

}
module.exports= UsuarioController;