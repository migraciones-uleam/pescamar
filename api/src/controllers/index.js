module.exports = {
    HomeController: require('./home.controller'),
    LabController: require('./lab.controller'),
    UsuarioController: require('./usuario.controller'),
    AreaController: require('./area.controller'),
    EquiController: require('./equi.controller')

}
